export const OpenConnection = nickname =>
  JSON.stringify({
    type: 'open-connection',
    nickname,
  })

export const ChatMessage = (nickname, message) =>
  JSON.stringify({
    type: 'chat-message',
    nickname,
    message,
  })
