import {
  ChatMessage,
  OpenConnection,
} from './messages.js'


document.querySelector('#login').onsubmit = event => {
  event.preventDefault()
  const nickname = event.target.nickname.value
  startChat(nickname)
}

const startChat = (nickname) => {
  document.querySelector('#login').classList.add('hidden')
  document.querySelector('#chat').classList.remove('hidden')

  const form = document.querySelector('#send')
  const text = document.querySelector('#text')

  const addMessage = ({ nickname, message }) => {
    const p = document.createElement('p')

    const user_tag = document.createElement('span')
    user_tag.classList.add('nickname')
    user_tag.textContent = nickname + ': '
    p.appendChild(user_tag)

    const message_tag = document.createElement('span')
    message_tag.textContent = message
    p.appendChild(message_tag)

    text.appendChild(p)
    p.scrollIntoView()
  }

  const addSystemMessage = ({ message }) => {
    const p = document.createElement('p')
    p.classList.add('system-message')
    p.textContent = message
    text.appendChild(p)
    p.scrollIntoView()
  }

  const addParticipants = ({ participants }) => {
    document.querySelector('#participants').textContent = 'Chat with ' + participants.join(', ')
  }

  const socket = new WebSocket('ws://localhost:3000')

  socket.addEventListener('open', () => {
    console.log('Websocket opened')
    socket.send(OpenConnection(nickname))
  })

  const message_handlers = {
    'open-connection': ({ nickname }) => addSystemMessage({
      message: `${nickname} joined the chat.`,
    }),
    'close-connection': ({ nickname }) => addSystemMessage({
      message: `${nickname} left the chat.`,
    }),
    'chat-message': addMessage,
    'welcome': addSystemMessage,
    'participants': addParticipants,
  }

  socket.addEventListener('message', event => {
    console.log(`Message received: ${event.data}`)
    const { type, ...data } = JSON.parse(event.data)
    message_handlers[type](data)
  })

  socket.addEventListener('close', () => {
    console.log('¡¡oY')
  })

  form.onsubmit = event => {
    event.preventDefault()
    const message = event.target.message.value
    event.target.message.value = ''
    socket.send(ChatMessage(nickname, message))
    addMessage({
      nickname,
      message,
    })
  }
}
