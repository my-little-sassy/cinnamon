'use strict'

const express = require('express')
const { Server } = require('ws')


const { PORT } = process.env

const app = express()

app.use(express.static('client'))

const listener = app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`)
})

const server = new Server({
  server: listener,
})

const broadcast = (message, exclude = []) => {
  server.clients.forEach(client => {
    if (exclude.includes(client)) return
    client.send(message)
  })
}

const connected = []

server.on('connection', socket => {
  let nickname = ''

  socket.on('message', message => {
    console.log(`Message received: ${message}`)
    const data = JSON.parse(message)
    if (data.type === 'open-connection') {
      nickname = data.nickname
      socket.send(JSON.stringify({
        type: 'welcome',
        message: `Welcome to the chat, ${nickname}`
      }))
      connected.push(nickname)
      broadcast(JSON.stringify({
        type: 'participants',
        participants: connected
      }))
    }
    broadcast(message, [ socket ])
  })

  socket.on('close', () => {
    connected.splice(connected.indexOf(nickname), 1)
    broadcast(JSON.stringify({
      type: 'close-connection',
      nickname,
    }))
    broadcast(JSON.stringify({
      type: 'participants',
      participants: connected
    }))
  })
})
